/**
 * Copyright (C) 2004 Billy Biggs <vektor@dumbterm.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef XRESVIEW_H_INCLUDED
#define XRESVIEW_H_INCLUDED

#include <gtk/gtk.h>
#include <X11/Xlib.h>

/**
 * This class provides a Gtk+ widget which displays information about
 * X server resources used by an application.
 */

typedef struct xresview_s xresview_t;

xresview_t *xresview_new( void );
void xresview_delete( xresview_t *rv );

void xresview_clear( xresview_t *rv, Display *dpy, Window root );
void xresview_load( xresview_t *rv, Display *dpy, Window win, Window root );
GtkWidget *xresview_get_widget( xresview_t *rv );

#endif /* XRESVIEW_H_INCLUDED */
