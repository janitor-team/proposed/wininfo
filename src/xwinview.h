/**
 * Copyright (C) 2004 Billy Biggs <vektor@dumbterm.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef XWINVIEW_H_INCLUDED
#define XWINVIEW_H_INCLUDED

#include <gtk/gtk.h>
#include <X11/Xlib.h>

/**
 * This class provides a Gtk+ widget which displays information about
 * a list of X window objects.
 */

typedef struct xwinview_s xwinview_t;

xwinview_t *xwinview_new( void );
void xwinview_delete( xwinview_t *wv );

void xwinview_clear( xwinview_t *wv, Display *dpy, Window root );
void xwinview_load( xwinview_t *wv, Display *dpy, Window win, Window root );
GtkWidget *xwinview_get_widget( xwinview_t *wv );

#endif /* XWINVIEW_H_INCLUDED */
